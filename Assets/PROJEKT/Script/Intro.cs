﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Intro : MonoBehaviour {

	private Queue<string> sentences;

	public Text dialogueText;

	// Use this for initialization
	void Start () {

		sentences = new Queue<string> ();
		
	}
	
	// Update is called once per frame
	public void StartDialogue (Dialogue dialogue) {

		sentences.Clear ();

		foreach (string sentence in dialogue.sentences) {

			sentences.Enqueue (sentence);
		}

		DisplayNextSentence ();

	}

	public void DisplayNextSentence(){

		if (sentences.Count == 0) {

			EndDialogue ();
			return;
		}

		string sentence = sentences.Dequeue ();
		StopAllCoroutines ();
		StartCoroutine (TypeSentence (sentence));
	}

	IEnumerator TypeSentence (string sentence){

		dialogueText.text = "";
		foreach (char letter in sentence.ToCharArray())
		{
			dialogueText.text += letter;
			yield return null;

	}
	}

	void EndDialogue(){

		Debug.Log ("End of Conversation");
	}
}
