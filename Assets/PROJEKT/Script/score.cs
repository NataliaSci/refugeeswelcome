﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class score : MonoBehaviour {

	public Slider immiBar;
	public int currentValue;
	private int maxvalue = 100;
	public GameObject Ui;
	public GameObject GameOver;


	void Start(){

		currentValue = 0;
		immiBar.maxValue = maxvalue;
	}

	void Update() {

		immiBar.value = currentValue;

		if (currentValue >= 100) {

			Ui.SetActive (false);
			GameOver.SetActive (true);
		}
	}
		
}