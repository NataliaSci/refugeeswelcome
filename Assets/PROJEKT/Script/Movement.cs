﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour {

	[SerializeField] [Range(1,20)]
	private float speed = 2;     
	private Vector3 targetPosition;
	private bool isMoving;

	const int LEFT_MOUSE_BUTTON = 0;
	private int points = 5;


	public GameObject targetObj;
	public GameObject signal;
	float timeLeft = 1.5f;


	// Use this for initialization
	void Start () {
		targetPosition = transform.position;
		isMoving = false;

	}
	
	// Update is called once per frame
	void Update () {
		float timeLeft = 1.5f;
		
		if (Input.GetMouseButton (LEFT_MOUSE_BUTTON))
			SetTargetPosition ();

		if (isMoving)
			MovePlayer ();

		if (Input.GetKeyDown (KeyCode.Space)) {

			signal.SetActive (true);
		} else if (Input.GetKeyUp (KeyCode.Space)) {
			signal.SetActive (false);
		}
	}


	void SetTargetPosition(){
		
		Plane plane = new Plane (Vector3.up, transform.position);
		Ray ray = Camera.main.ScreenPointToRay (Input.mousePosition);
		float point = 0f;

		if (plane.Raycast (ray, out point))
			targetPosition = ray.GetPoint (point);

		isMoving = true;

	}

	void MovePlayer(){
		
		transform.LookAt (targetPosition);
		transform.position = Vector3.MoveTowards(transform.position, targetPosition, speed * Time.deltaTime);

		if(transform.position == targetPosition)
			isMoving = false;
		Debug.DrawLine (transform.position, targetPosition, Color.red);

	}

	void OnTriggerEnter (Collider coll){

		if (coll.gameObject.tag == "Human") {

			targetObj.GetComponent<score> ().currentValue += points;
			Destroy (coll.gameObject);

		}
	}
}
