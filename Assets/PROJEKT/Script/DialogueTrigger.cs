﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DialogueTrigger : MonoBehaviour {

	public Dialogue dialogue;

	public GameObject InterfaceBox;
	public GameObject StartButton;
	public GameObject StartGameButton;
	public GameObject StartImage;

	public void TriggerDialogue(){

		FindObjectOfType<Intro> ().StartDialogue (dialogue);
		InterfaceBox.SetActive (true);
		StartButton.SetActive (false);
		StartGameButton.SetActive (true);
		StartImage.SetActive (false);
	}
}
