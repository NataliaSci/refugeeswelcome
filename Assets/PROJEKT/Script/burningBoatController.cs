﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class burningBoatController : MonoBehaviour {


	public Transform target;
	private GameObject targetObj;
	public float speed;

	public GameObject human;
	public GameObject human2;
	public GameObject human3;

	public GameObject effect;

	public int point1 = 20;
	float timeLeft = 10.0f;

	Vector3 pos;


		// Use this for initialization
	void Start () {

		targetObj = GameObject.Find ("Platform");
		target = targetObj.transform;
	
	}

	void Update() {

		timeLeft -= Time.deltaTime;
		float step = speed * Time.deltaTime;
		transform.position = Vector3.MoveTowards(transform.position, target.position, step);

		if (timeLeft < 0) {


			Instantiate (human, transform.position * 1.0f, transform.rotation);
			Instantiate (human2, transform.position * 1.07f, transform.rotation);
			Instantiate (human3, transform.position * 1.16f, transform.rotation);

			Destroy (gameObject);

		}
	}

	void OnTriggerEnter (Collider coll){

		if (coll.gameObject.tag == "Platform") {

			targetObj.GetComponent<score> ().currentValue += point1;
			Destroy (gameObject);

			}

		if (coll.gameObject.tag == "Signal") {

			Destroy(gameObject);
			Instantiate (effect, transform.position, transform.rotation);

		}
		}
	}