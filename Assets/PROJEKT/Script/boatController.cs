﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class boatController : MonoBehaviour {


	public Transform target;
	private GameObject targetObj;
	public float speed;

	public int point1 = 10;

	public GameObject effect;


	// Use this for initialization
	void Start () {
		
		targetObj = GameObject.Find ("Platform");
		target = targetObj.transform;


	}
	
	void Update() {

		float step = speed * Time.deltaTime;
		transform.position = Vector3.MoveTowards (transform.position, target.position, step);
	}

	void OnTriggerEnter (Collider coll){

		if (coll.gameObject.tag == "Platform") {

			targetObj.GetComponent<score> ().currentValue += point1;
			
			Destroy(gameObject);

		}

		if (coll.gameObject.tag == "Signal") {

			Destroy(gameObject);
			Instantiate (effect, transform.position, transform.rotation);
		}
	}
}
