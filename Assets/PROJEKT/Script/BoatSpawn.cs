﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoatSpawn : MonoBehaviour {

		
		public GameObject boat1;               
		public GameObject boat2;
		public float spawnTime = 3f;            // How long between each spawn.
		public Transform[] spawnPoints;         // An array of the spawn points this enemy can spawn from.
		public Transform[] spawnPoints2;


		void Start ()
		{
			// Call the Spawn function after a delay of the spawnTime and then continue to call after the same amount of time.
			InvokeRepeating ("Spawn", spawnTime, spawnTime);
		}
		

		void Spawn ()
		{
		
			// Find a random index between zero and one less than the number of spawn points.
			int spawnPointIndex = Random.Range (0, spawnPoints.Length);
		    int spawnPointIndex2 = Random.Range (0, spawnPoints2.Length);

			// Create an instance of the prefab at the randomly selected spawn point's position and rotation.
			Instantiate (boat1, spawnPoints[spawnPointIndex].position, spawnPoints[spawnPointIndex].rotation);
		    Instantiate (boat2, spawnPoints2[spawnPointIndex2].position, spawnPoints2[spawnPointIndex2].rotation);
		}
	}
